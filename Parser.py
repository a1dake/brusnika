import sys
import requests
import urllib3
from urllib3 import exceptions
from datetime import datetime, timedelta
sys.path.append("/home/manage_report")

from Send_report.mywrapper import magicDB

class Parser:
    def __init__(self, parser_name: str):
        self.result_data: dict = {'name': parser_name,
                                  'data': []}

    @magicDB
    def run(self):
        content: list = self.get_content()
        self.result_data['data'] = content
        print(content)
        return self.result_data

    # Получение данных
    def get_data(self):
        date_format = '%d.%m.%Y %H:%M'
        today = datetime.now()
        yesterday = today - timedelta(days=1)
        tomorrow = today + timedelta(days=1)

        from_date: str = yesterday.strftime(date_format)
        to_date: str = tomorrow.strftime(date_format)
        print('date from: {}, to: {}'.format(from_date, to_date))

        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        session = requests.Session()
        url = 'https://srm.brusnika.ru/graphql?operation=tradeSearch'
        session.headers.update({
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.56'
        })
        session.get('https://srm.brusnika.ru/frame/index.html', verify=False)

        json_data = {'operationName': "tradeSearch", "variables":{'limit':400, "skip":0, "tradeQueryDto":{"order":{"expressions":[{"ascending":False, "property": "REGISTERED_DATE"}, {"property": "ID", "ascending":False}]}, "statusCode":{"mode": "IN", "values":["active"]}, "processStatuses":["BEFORE_BID_SUBMISSION", "QUALIFICATION", "QUALIFICATION_FINISHED", "PARTICIPATION_CONFIRMATION", "OFFER_SUBMISSION", "CONSIDERATION"], "procurementMethod":{"mode": "IN", "values":[{"id":100806960}, {"id":102349783}]}, "sitemapPage": "purchases.search"}}, "query": "query tradeSearch($tradeQueryDto: TradeQueryDtoInput, $limit: Int, $skip: Int) {\n  trades(tradeQueryDto: $tradeQueryDto, limit: $limit, skip: $skip) {\n    items {\n      id\n      enableQualification\n      qualification {\n        id\n        startDate\n        endDate\n        __typename\n      }\n      viewPageAvailable\n      title\n      request\n      registeredNumber\n      bidSubmissionStartDate\n      bidSubmissionEndDate\n      smallBusinessPurchase\n      signatureRequired\n      processStatus\n      lifecycle {\n        id\n        currentStage {\n          id\n          title\n          startDate\n          finishDate\n          plannedFinishDate\n          current\n          subStageTitle\n          __typename\n        }\n        __typename\n      }\n      currentStage {\n        id\n        __typename\n      }\n      acceptance {\n        ...TradeSearchAcceptance\n        __typename\n      }\n      currentCommitteeSessionRequest {\n        id\n        acceptance {\n          ...TradeSearchAcceptance\n          __typename\n        }\n        __typename\n      }\n      targetUser {\n        ...UserReference\n        __typename\n      }\n      authorizedPersons {\n        id\n        type {\n          id\n          group {\n            id\n            __typename\n          }\n          __typename\n        }\n        user {\n          ...UserReference\n          __typename\n        }\n        __typename\n      }\n      currency {\n        id\n        title\n        code\n        letterCode\n        __typename\n      }\n      organizer {\n        id\n        title\n        types {\n          id\n          code\n          __typename\n        }\n        departmentTopOrganization {\n          id\n          title\n          __typename\n        }\n        __typename\n      }\n      customer {\n        id\n        title\n        types {\n          id\n          code\n          __typename\n        }\n        departmentTopOrganization {\n          id\n          title\n          __typename\n        }\n        __typename\n      }\n      destinationRegion {\n        id\n        title\n        __typename\n      }\n      procurementMethod {\n        id\n        code\n        title\n        __typename\n      }\n      lots {\n        id\n        title\n        number\n        initialContractPrice\n        detailedPermissions {\n          createTradeFromLot {\n            granted\n            error\n            __typename\n          }\n          confirmParticipation {\n            ...PermissionDetail\n            __typename\n          }\n          rejectParticipation {\n            ...PermissionDetail\n            __typename\n          }\n          __typename\n        }\n        trade {\n          id\n          registeredNumber\n          request\n          currency {\n            id\n            title\n            code\n            letterCode\n            __typename\n          }\n          organizer {\n            id\n            title\n            types {\n              id\n              code\n              __typename\n            }\n            departmentTopOrganization {\n              id\n              title\n              __typename\n            }\n            __typename\n          }\n          customer {\n            id\n            title\n            types {\n              id\n              code\n              __typename\n            }\n            departmentTopOrganization {\n              id\n              title\n              __typename\n            }\n            __typename\n          }\n          destinationRegion {\n            id\n            title\n            __typename\n          }\n          procurementMethod {\n            id\n            code\n            title\n            __typename\n          }\n          lots {\n            id\n            __typename\n          }\n          currentStage {\n            id\n            tradeResult {\n              id\n              lotResults {\n                id\n                lot {\n                  id\n                  detailedPermissions {\n                    confirmParticipation {\n                      denied\n                      error\n                      granted\n                      __typename\n                    }\n                    __typename\n                  }\n                  __typename\n                }\n                __typename\n              }\n              __typename\n            }\n            __typename\n          }\n          __typename\n        }\n        biddingData {\n          id\n          bidSubmissionEndDate\n          __typename\n        }\n        imageAttachments {\n          ...FileDetail\n          __typename\n        }\n        __typename\n      }\n      procurementClassifier {\n        id\n        code\n        title\n        __typename\n      }\n      committeeSessionRequests {\n        id\n        detailedPermissions {\n          createCommitteeSession {\n            ...PermissionDetail\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    total\n    __typename\n  }\n}\n\nfragment TradeSearchAcceptance on Acceptance {\n  id\n  ownerStep {\n    id\n    currentExecutor {\n      ...UserReference\n      __typename\n    }\n    __typename\n  }\n  currentUserStep {\n    id\n    currentExecutor {\n      ...UserReference\n      __typename\n    }\n    task {\n      id\n      createdAt\n      mustCompleteBefore\n      __typename\n    }\n    __typename\n  }\n  __typename\n}\n\nfragment UserReference on User {\n  id\n  title\n  abbreviatedTitle\n  email\n  phone\n  position\n  organization {\n    id\n    title\n    __typename\n  }\n  __typename\n}\n\nfragment PermissionDetail on PermissionEvaluationResult {\n  denied\n  error\n  granted\n  __typename\n}\n\nfragment FileDetail on File {\n  id\n  name\n  icon\n  size\n  href\n  token\n  uploadToken\n  date\n  documentType {\n    id\n    code\n    title\n    __typename\n  }\n  signatures {\n    id\n    signer\n    signedBy {\n      id\n      __typename\n    }\n    certificate {\n      fingerprints {\n        sha1\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  thumbnail\n  signatureToken\n  signatureUploadToken\n  mimeType {\n    id\n    code\n    __typename\n  }\n  generating\n  __typename\n}\n"}
        response = session.post(url, json=json_data, verify=False)
        data = response.json()
        json_data = data.get("data")
        trades = json_data.get("trades")
        list_id = []
        for item in trades.get("items"):
            start_date = datetime.fromtimestamp(item.get("bidSubmissionStartDate") / 1000)
            if yesterday < start_date:
                tender_id = item.get("id")
                list_id.append(tender_id)
        return list_id

    def get_content(self):
        data = []
        list_id = self.get_data()
        session = requests.Session()
        url = 'https://srm.brusnika.ru/graphql?operation=getTradeInfo'
        session.headers.update({
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.56'
        })
        session.get('https://srm.brusnika.ru/frame/index.html', verify=False)
        for tender_id in list_id:
            json_data = {"operationName":"getTradeInfo","variables":{"id": f'{tender_id}'},"query":"query getTradeInfo($id: Long) {\n  trade(id: $id) {\n    id\n    title\n    registeredNumber\n    initialized\n    initialPriceRequestText\n    hasBids\n    status {\n      id\n      code\n      __typename\n    }\n    published\n    request\n    viewPageAvailable\n    enableIntegrationERP\n    organizer {\n      ...OrganizationComponentFragment\n      __typename\n    }\n    customer {\n      ...OrganizationComponentFragment\n      __typename\n    }\n    targetUser {\n      ...UserReference\n      __typename\n    }\n    customerRepresentative {\n      ...UserReference\n      __typename\n    }\n    purchaseRequestDate\n    deliveryDate\n    participationNotes\n    bidValidityRules\n    financialAuthority {\n      ...OrganizationComponentFragment\n      __typename\n    }\n    basedOnRequest {\n      ...TradeReference\n      __typename\n    }\n    basedOnRequests {\n      id\n      title\n      registeredNumber\n      targetUser {\n        ...UserReference\n        __typename\n      }\n      __typename\n    }\n    baseForTrades {\n      id\n      title\n      registeredNumber\n      __typename\n    }\n    divisions {\n      id\n      code\n      title\n      __typename\n    }\n    permissions\n    detailedPermissions {\n      ...TradeDetailedPermissionsDetail\n      __typename\n    }\n    contact\n    contactWithPosition\n    email\n    phone\n    additionalContacts\n    procurementMethod {\n      id\n      code\n      title\n      __typename\n    }\n    decryptor {\n      id\n      code\n      title\n      __typename\n    }\n    tradeType {\n      id\n      code\n      title\n      __typename\n    }\n    destinationRegion {\n      id\n      code\n      title\n      __typename\n    }\n    authorizedPersons {\n      id\n      type {\n        id\n        code\n        title\n        group {\n          id\n          __typename\n        }\n        __typename\n      }\n      user {\n        ...UserReference\n        __typename\n      }\n      phone\n      email\n      __typename\n    }\n    smallBusinessPurchase\n    participationForm\n    tradeResultSubscribers {\n      ...OrganizationComponentFragment\n      __typename\n    }\n    invitedOrganizationsPartialResults {\n      total\n      __typename\n    }\n    signatureRequired\n    closedEnvelopesVisibilityMode\n    qualification {\n      id\n      startDate\n      endDate\n      __typename\n    }\n    defineBidSubmissionStartDate\n    qualificationRequirements\n    requireQualificationAttachments\n    qualificationDocumentationAttachments {\n      ...FileDetail\n      __typename\n    }\n    purchaseDocumentationTemplateAttachments {\n      ...FileDetail\n      __typename\n    }\n    purchaseDocumentationAttachments {\n      ...FileDetail\n      __typename\n    }\n    mtoRequestAttachments {\n      ...FileDetail\n      __typename\n    }\n    initialPriceCalcAttachments {\n      ...FileDetail\n      __typename\n    }\n    contractRequirementsAttachments {\n      ...FileDetail\n      __typename\n    }\n    extraAttachments {\n      ...FileDetail\n      __typename\n    }\n    contractorCheckDocsAttachments {\n      ...FileDetail\n      __typename\n    }\n    participationConfirmationEndDate\n    bidSubmissionStartDate\n    bidSubmissionEndDate\n    tradeStartDate\n    tradeEndDate\n    forcedRebidding\n    rebiddingStartDate\n    examinationPlace\n    completionPlace\n    firstPartsExaminationDate\n    secondPartsExaminationDate\n    examinationDate\n    completionDate\n    tradeResultsRelevancyDate\n    vatType\n    documentationVisibility\n    currency {\n      id\n      code\n      title\n      __typename\n    }\n    singleSupplierReasons {\n      id\n      code\n      title\n      __typename\n    }\n    tradeReasoning\n    urgentTradeReasoning {\n      ...FileDetail\n      __typename\n    }\n    lots {\n      id\n      currency {\n        id\n        code\n        title\n        __typename\n      }\n      vatType\n      hasBids\n      hasNotCanceledBids\n      hasBidsWithDifferentQuantity\n      number\n      title\n      goodsDescription\n      bidPriceStep\n      bidPriceStepFrom\n      bidPriceStepTo\n      bidItemPriceStep\n      bidItemPriceStepFrom\n      bidItemPriceStepTo\n      priceStepPercents\n      priceStepPercentsFrom\n      priceStepPercentsTo\n      priceStepMode\n      targetPrice\n      initialContractPrice\n      hideInitialPrice\n      enablePurchaseItems\n      enableManufacturerPerPurchaseItem\n      goodsRequirements\n      quantityPerPurchaseItem\n      enablePricePerPurchaseItem\n      enableCoefficientTrade\n      executionPeriod\n      enableBidAssurance\n      assuranceAmount\n      assuranceMode\n      bidAssuranceCurrency {\n        id\n        code\n        title\n        __typename\n      }\n      requiredContractors\n      allowQuantityInBid\n      allowCurrencyInBid\n      allowSupplierStateInBid\n      bidScanCopyRequired\n      allowManufacturerInBid\n      allowAnalogueInBid\n      allowPartialBids\n      allowDeliveryPeriodInBid\n      allowAlternativeBids\n      autoInvitationByPriceEnabled\n      mustProlongate\n      prolongationMinutes\n      maxProlongations\n      bidPriceValidationMode\n      transportChargesMode\n      submissionForm {\n        id\n        code\n        title\n        __typename\n      }\n      purchaseItems {\n        id\n        sectionNumber\n        sectionTitle\n        title\n        productTitle\n        code\n        productClassifiers {\n          id\n          code\n          title\n          unit {\n            id\n            code\n            title\n            __typename\n          }\n          __typename\n        }\n        quantity\n        unit {\n          id\n          code\n          title\n          __typename\n        }\n        price\n        targetPrice\n        withoutTransportCharges\n        consumptionRate\n        estimatedGoodsPrice\n        estimatedWorkPrice\n        fixGoodsPrice\n        fixWorksPrice\n        comment\n        characteristics\n        deliveryPeriod\n        deliveryPlace\n        manufacturer\n        workType {\n          id\n          code\n          title\n          unit {\n            id\n            code\n            title\n            __typename\n          }\n          workCharacteristics {\n            id\n            code\n            title\n            __typename\n          }\n          __typename\n        }\n        workCharacteristic {\n          id\n          code\n          title\n          __typename\n        }\n        productType {\n          id\n          code\n          title\n          __typename\n        }\n        gost\n        customer\n        consumer\n        cargoReceiver\n        otherMaterialsCoefficient\n        priceAdjustmentCoefficient\n        totalCostPercentString\n        minimalQuantity\n        integrationSubjects {\n          id\n          partner\n          __typename\n        }\n        __typename\n      }\n      supplier {\n        ...OrganizationComponentFragment\n        __typename\n      }\n      imageAttachments {\n        ...FileDetail\n        __typename\n      }\n      contractDraftAttachments {\n        ...FileDetail\n        __typename\n      }\n      technicalSpecificationAttachments {\n        ...FileDetail\n        __typename\n      }\n      mainMaterialsAttachments {\n        ...FileDetail\n        __typename\n      }\n      workScheduleAttachments {\n        ...FileDetail\n        __typename\n      }\n      cashFlowTimeLineAttachments {\n        ...FileDetail\n        __typename\n      }\n      projectDocumentationAttachments {\n        ...FileDetail\n        __typename\n      }\n      otherAttachments {\n        ...FileDetail\n        __typename\n      }\n      initialUnitPriceSum\n      unitPriceFrameContract\n      enablePeriodPerPurchaseItem\n      currentLotData {\n        id\n        enableContractAssurance\n        contractAssurance\n        contractAssuranceMode\n        __typename\n      }\n      executionPeriodDays\n      calculateEffectivePriceByExecutionPeriod\n      processStatus\n      lifecycle {\n        id\n        currentStage {\n          id\n          title\n          startDate\n          finishDate\n          plannedFinishDate\n          current\n          subStageTitle\n          __typename\n        }\n        __typename\n      }\n      enableExecutionPeriodStages\n      executionPeriodStages {\n        id\n        title\n        durationDays\n        employeeQuantity\n        __typename\n      }\n      detailedPermissions {\n        ...LotDetailedPermissions\n        __typename\n      }\n      enableAuction\n      auctionData {\n        ...AuctionDataInfo\n        __typename\n      }\n      objectArea\n      objectCharacteristics {\n        id\n        title\n        value\n        __typename\n      }\n      objectCode\n      projectAddress\n      projectClassifier {\n        id\n        code\n        title\n        __typename\n      }\n      procurementClassifier {\n        id\n        code\n        title\n        __typename\n      }\n      customerPrice\n      estimatedPriceSource\n      enableTotalCostPercent\n      __typename\n    }\n    projectClassifier {\n      id\n      title\n      __typename\n    }\n    prepaymentTerms\n    guaranteePeriodReserveAssurance\n    guaranteePeriodReserveAssuranceTerms\n    deadlineExecutionBonus\n    guaranteePeriod\n    termsNotes\n    enableRankingCriteria\n    rankingScenario {\n      id\n      code\n      title\n      rankingCriteria {\n        id\n        code\n        __typename\n      }\n      __typename\n    }\n    rankingCriteriaBindings {\n      id\n      max\n      min\n      weight\n      unit {\n        id\n        code\n        title\n        __typename\n      }\n      rankingCriteria {\n        id\n        code\n        title\n        comment\n        __typename\n      }\n      children {\n        id\n        max\n        min\n        weight\n        unit {\n          id\n          code\n          title\n          __typename\n        }\n        rankingCriteria {\n          id\n          code\n          title\n          comment\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    procurementClassifier {\n      id\n      code\n      title\n      __typename\n    }\n    primaryProcurementCategory {\n      id\n      code\n      title\n      __typename\n    }\n    qualifyingRequirements\n    participationConditions\n    contractSubjectType {\n      id\n      title\n      code\n      __typename\n    }\n    contractExecutionDate\n    purchaseDestination\n    purchaseTerms\n    purchaseTermsComment\n    deliveryPeriod\n    notes\n    contractDraftAttachments {\n      ...FileDetail\n      __typename\n    }\n    technicalSpecificationAttachments {\n      ...FileDetail\n      __typename\n    }\n    tenderOfferFormAttachments {\n      ...FileDetail\n      __typename\n    }\n    projectDocumentationAttachments {\n      ...FileDetail\n      __typename\n    }\n    otherAttachments {\n      ...FileDetail\n      __typename\n    }\n    modifications {\n      id\n      modificationDate\n      modificationData\n      __typename\n    }\n    currentStage {\n      id\n      number\n      __typename\n    }\n    tradePauseDurationWorkingDays\n    commonLotData {\n      id\n      lot {\n        id\n        __typename\n      }\n      purchaseItems {\n        id\n        __typename\n      }\n      bidExtensionFields {\n        id\n        __typename\n      }\n      allowGuaranteeRequirementsInBid\n      generalContractingPercentage\n      paymentTermsType\n      paymentDelayTerms\n      textPaymentTerms\n      allowPaymentStagesInBid\n      allowPaymentDelayTermsInBid\n      paymentStagesPeriodInTrade\n      paymentStagesPercentsInTrade\n      paymentStages {\n        id\n        terms\n        type {\n          id\n          code\n          title\n          __typename\n        }\n        period\n        periodDays\n        percents\n        paymentForm\n        __typename\n      }\n      multipleWinnersAllowed\n      requireBidItems\n      russianManufacturePriority\n      __typename\n    }\n    prepaymentOffsetTerms\n    priceStructure\n    allowSelfDelivery\n    selfDeliveryExpenses {\n      id\n      selfDeliveryPlace\n      amount\n      __typename\n    }\n    supplierRecommendations {\n      id\n      emails\n      phone\n      title\n      __typename\n    }\n    scheduledAutogeneration\n    budgetDetails\n    projectAddress\n    submissionStages {\n      id\n      number\n      endDate\n      __typename\n    }\n    initialContractPriceCalculationNotes\n    unplannedReason\n    unplanned\n    requireRnpAbsence\n    integrationSubjects {\n      id\n      partner\n      __typename\n    }\n    minimalTradeTimeAfterBid\n    winnersSourceTrade {\n      id\n      registeredNumber\n      title\n      __typename\n    }\n    initialPriceRequests {\n      id\n      title\n      registeredNumber\n      registeredDate\n      published\n      currentStage {\n        id\n        __typename\n      }\n      __typename\n    }\n    tradeCancels {\n      id\n      attachments {\n        ...FileDetail\n        __typename\n      }\n      cancelDate\n      lots {\n        id\n        number\n        __typename\n      }\n      reasoning\n      __typename\n    }\n    lifecycle {\n      ...TradeLifecycleDetail\n      __typename\n    }\n    purchaseMethod\n    ...BlockQuantitiesItemDocumentTrade\n    ...BidExtensionTradeInfo\n    ...TradeExtensionDocumentTrade\n    ...ParticipantCheckTypesTrade\n    ...DevelopmentTrade\n    __typename\n  }\n}\n\nfragment OrganizationComponentFragment on Organization {\n  ...OrganizationReference\n  superOrganizations {\n    ...OrganizationReference\n    __typename\n  }\n  topSuperOrganization {\n    ...OrganizationReference\n    __typename\n  }\n  departmentTopOrganization {\n    ...OrganizationReference\n    __typename\n  }\n  __typename\n}\n\nfragment OrganizationReference on Organization {\n  id\n  title\n  types {\n    code\n    id\n    title\n    __typename\n  }\n  inn\n  taxCode\n  taxCodeTitle\n  ogrn\n  registrationCode\n  registrationCodeTitle\n  smallBusinessOrganization\n  currentOrganizationData {\n    id\n    city\n    __typename\n  }\n  email\n  phone\n  regionClassifier {\n    id\n    code\n    title\n    fullTitle\n    area {\n      id\n      code\n      title\n      fullTitle\n      __typename\n    }\n    country {\n      id\n      code\n      title\n      fullTitle\n      __typename\n    }\n    __typename\n  }\n  __typename\n}\n\nfragment UserReference on User {\n  id\n  title\n  abbreviatedTitle\n  email\n  phone\n  position\n  organization {\n    id\n    title\n    __typename\n  }\n  __typename\n}\n\nfragment TradeReference on Trade {\n  id\n  viewPageAvailable\n  procurementMethod {\n    id\n    code\n    title\n    __typename\n  }\n  registeredNumber\n  title\n  __typename\n}\n\nfragment TradeDetailedPermissionsDetail on TradePermissions {\n  editBidSubmissionDates {\n    ...PermissionDetail\n    __typename\n  }\n  editEnableQualification {\n    ...PermissionDetail\n    __typename\n  }\n  viewDocumentationVisibility {\n    ...PermissionDetail\n    __typename\n  }\n  createTradeFromRequest {\n    ...PermissionDetail\n    __typename\n  }\n  cancel {\n    ...PermissionDetail\n    __typename\n  }\n  startBiddingProtocol {\n    ...PermissionDetail\n    __typename\n  }\n  startConsiderationProtocol {\n    ...PermissionDetail\n    __typename\n  }\n  startCompletionProtocol {\n    ...PermissionDetail\n    __typename\n  }\n  startFirstPartsConsiderationProtocol {\n    ...PermissionDetail\n    __typename\n  }\n  startSecondPartsConsiderationProtocol {\n    ...PermissionDetail\n    __typename\n  }\n  startResultsCommitteeProtocol {\n    ...PermissionDetail\n    __typename\n  }\n  createDocumentationCommitteeRequest {\n    ...PermissionDetail\n    __typename\n  }\n  createFreeQuestionCommitteeRequest {\n    ...PermissionDetail\n    __typename\n  }\n  publishOnExternalSystem {\n    ...PermissionDetail\n    __typename\n  }\n  createInitialPriceRequest {\n    ...PermissionDetail\n    __typename\n  }\n  uploadExtraAttachments {\n    ...PermissionDetail\n    __typename\n  }\n  registerExplanation {\n    ...PermissionDetail\n    __typename\n  }\n  registerOuterExplanation {\n    ...PermissionDetail\n    __typename\n  }\n  publishQualificationBid {\n    ...PermissionDetail\n    __typename\n  }\n  prolongate {\n    ...PermissionDetail\n    __typename\n  }\n  prolongateQualification {\n    ...PermissionDetail\n    __typename\n  }\n  viewFullInfo {\n    ...PermissionDetail\n    __typename\n  }\n  __typename\n}\n\nfragment PermissionDetail on PermissionEvaluationResult {\n  denied\n  error\n  granted\n  __typename\n}\n\nfragment FileDetail on File {\n  id\n  name\n  icon\n  size\n  href\n  token\n  uploadToken\n  date\n  documentType {\n    id\n    code\n    title\n    __typename\n  }\n  signatures {\n    id\n    signer\n    signedBy {\n      id\n      __typename\n    }\n    certificate {\n      fingerprints {\n        sha1\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  thumbnail\n  signatureToken\n  signatureUploadToken\n  mimeType {\n    id\n    code\n    __typename\n  }\n  generating\n  __typename\n}\n\nfragment LotDetailedPermissions on LotPermissions {\n  createTradeFromLot {\n    ...PermissionDetail\n    __typename\n  }\n  viewPrice {\n    ...PermissionDetail\n    __typename\n  }\n  confirmParticipation {\n    ...PermissionDetail\n    __typename\n  }\n  rejectParticipation {\n    ...PermissionDetail\n    __typename\n  }\n  __typename\n}\n\nfragment AuctionDataInfo on AuctionData {\n  id\n  step\n  stepPercents\n  stepMode\n  __typename\n}\n\nfragment TradeLifecycleDetail on Lifecycle {\n  id\n  stages {\n    id\n    stageId\n    title\n    current\n    startDate\n    finishDate\n    __typename\n  }\n  currentStage {\n    id\n    stageId\n    title\n    current\n    startDate\n    finishDate\n    __typename\n  }\n  finished\n  __typename\n}\n\nfragment BlockQuantitiesItemDocumentTrade on Trade {\n  lots {\n    enableQuantityPerBlocks\n    purchaseItems {\n      blockQuantities\n      __typename\n    }\n    blocks {\n      id\n      number\n      title\n      __typename\n    }\n    __typename\n  }\n  __typename\n}\n\nfragment BidExtensionTradeInfo on Trade {\n  bidExtensionFields {\n    ...ExtensionFieldDetail\n    __typename\n  }\n  __typename\n}\n\nfragment ExtensionFieldDetail on ExtensionField {\n  id\n  order\n  required\n  title\n  unit\n  type\n  documentType {\n    id\n    title\n    code\n    __typename\n  }\n  options {\n    id\n    title\n    __typename\n  }\n  comment\n  extensionFieldCategory {\n    id\n    code\n    title\n    __typename\n  }\n  __typename\n}\n\nfragment TradeExtensionDocumentTrade on Trade {\n  enableQualification\n  qualificationExtensionFields {\n    ...ExtensionFieldDetail\n    __typename\n  }\n  __typename\n}\n\nfragment ExtensionFieldDetail on ExtensionField {\n  id\n  order\n  required\n  title\n  unit\n  type\n  documentType {\n    id\n    title\n    code\n    __typename\n  }\n  options {\n    id\n    title\n    __typename\n  }\n  comment\n  extensionFieldCategory {\n    id\n    code\n    title\n    __typename\n  }\n  __typename\n}\n\nfragment ParticipantCheckTypesTrade on Trade {\n  participantCheckTypes {\n    id\n    code\n    title\n    __typename\n  }\n  __typename\n}\n\nfragment DevelopmentTrade on Trade {\n  lots {\n    purchasePeriod\n    sellingArea\n    workEstimate\n    allowExtraWork\n    __typename\n  }\n  __typename\n}\n"}
            response = session.post(url, json=json_data, verify=False)
            tender_data = response.json()
            content = self.get_tender(tender_data)
            data.append(content)
        return data

    def get_tender(self, data):
        try:
            item_data = {
                'type': 2,
                'title': '',
                'purchaseNumber': '',
                'fz': 'Коммерческие',
                'purchaseType': '',
                'url': '',
                'lots': [],
                'attachments': [],
                'procedureInfo': {
                    'endDate': ''
                },
                'customer': {
                    'fullName': '',
                    'factAddress': '',
                    'inn': 0,
                    'kpp': 0,
                },
                'contactPerson': {
                    'lastName': '',
                    'firstName': '',
                    'middleName': '',
                    'contactPhone': '',
                    'contactEMail': '',
            }}
            data = data.get('data').get('trade')
            item_data['purchaseNumber'] = str(self.get_purchase_number(data))
            item_data['title'] = str(self.get_title(data))
            item_data['purchaseType'] = str(self.get_purchase_type(data))
            item_data['customer']['factAddress'] = str(self.get_adress(data))
            item_data['customer']['fullName'] = str(self.get_full_name(data))
            item_data['customer']['inn'] = int(self.get_inn(data))
            item_data['procedureInfo']['endDate'] = self.get_end_date(data)
            last_name, first_name, middle_name = self.get_name(data)
            item_data['contactPerson']['lastName'] = str(last_name)
            item_data['contactPerson']['firstName'] = str(first_name)
            item_data['contactPerson']['middleName'] = str(middle_name)
            phone, email = self.get_contacts(data)
            item_data['contactPerson']['contactPhone'] = str(phone)
            item_data['contactPerson']['contactEMail'] = str(email)

            item_data['url'] = 'https://srm.brusnika.ru/trades/' + str(self.get_purchase_number(data)) + '/info'
            lots = self.get_lots(data)
            item_data['lots'].append(lots)

            return item_data
        except Exception as e:
            print(f'{e} - Ошибка в получении данных')

    def get_title(self, data):
        try:
            title = ' '.join(data.get("title").split())
        except:
            title = ''
        return title

    def get_purchase_type(self, data):
        try:
            method = data.get("procurementMethod")
            purchase_type = method.get("title")
        except:
            purchase_type = ''
        return purchase_type

    def get_purchase_number(self, data):
        try:
            number = data.get("id")
        except:
            number = ''
        return number

    def get_end_date(self, data):
        try:
            date = datetime.fromtimestamp(data.get("bidSubmissionEndDate") / 1000).strftime("%d.%m.%y")
            formated_date = self.formate_date(date)
        except:
            formated_date = None
        return formated_date

    def get_adress(self, data):
        try:
            adress_data: dict | None = data.get("destinationRegion")[0]
            adress = adress_data.get("title")
            if adress is None:
                adress = ''
            else:
                adress = ''.join(adress)
        except:
            adress = ''
        return adress

    def get_full_name(self, data):
        try:
            organizer_data = data.get("organizer")
            name = organizer_data.get("title")
        except:
            name = ''
        return name

    def get_inn(self, data):
        try:
            organizer_data = data.get("organizer")
            inn = organizer_data.get("inn")
            if not inn or inn is None:
                inn = 0
        except:
            inn = 0
        return inn

    def get_name(self, data):
        try:
            name_data: str | None = data.get("contact", None)
            if name_data is None:
                last_name, first_name, middle_name = ""
            else:
                try:
                    name = name_data.split(' ')
                    try:
                        last_name = name[0]
                    except:
                        last_name = ''
                    try:
                        first_name = name[1]
                    except:
                        first_name = ''
                    try:
                        middle_name = name[2]
                    except:
                        middle_name = ''
                except:
                    last_name, first_name, middle_name = ""
        except:
            last_name, first_name, middle_name = ""

        return last_name, first_name, middle_name

    def get_contacts(self, data):
        contact_data: dict | None = data.get("targetUser", None)
        if isinstance(contact_data, dict):
            try:
                phone = contact_data.get('phone')
                if phone is None:
                    phone = ''
            except:
                phone = ''
            try:
                email = contact_data.get('email')
                if email is None:
                    email = ''
            except:
                email = ''
        else:
            phone = ''
            email = ''
        return phone, email

    def get_lots(self, data):
        try:
            lots_data = []
            lots = {
                'region': '',
                'price': '',
                'lotItems': []
            }
            start_price = 0

            try:
                adress_data: dict | None = data.get("destinationRegion")[0]
                adress = str(adress_data.get("title"))
                if adress is None:
                    adress = ''
                else:
                    adress = adress
            except:
                adress = ''

            lots['region'] = ' '.join(adress.split())

            for lot in data.get("lots"):

                price = lot.get("initialContractPrice")
                if price is None:
                    pass
                else:
                    start_price += int(price)

                name = str(lot.get("title"))
                if name is None:
                    result_name = ''
                else:
                    result_name = ' '.join(name.split())

                names = {
                    'code': str(lot.get("number")),
                    'name': result_name}

                lots['lotItems'].append(names)

            lots['price'] = start_price
            lots_data.append(lots)
        except:
            return []
        return lots_data

    def formate_date(self, old_date):
        date_object = datetime.strptime(old_date, '%d.%m.%y')
        formatted_date = date_object.strftime('%H.%M.%S %d.%m.%Y')
        return formatted_date
